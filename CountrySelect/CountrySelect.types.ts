export interface CountrySelectProps {
  __typename?: string;
}

export interface CountrySelectClasses {
  root: string;
  contentContainer: string;
  message: string;
  countryButton: string;
  closeButton: string;
  flag: string;
}

export declare type CountrySelectClassKey = keyof CountrySelectClasses;
