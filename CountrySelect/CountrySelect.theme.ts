import { Theme, ThemeOptions, ComponentsOverrides, ComponentsVariants } from '@mui/material/styles';

// https://mui.com/customization/theme-components/#default-props
export const defaultProps = {};

// https://mui.com/customization/theme-components/#global-style-overrides
export const styleOverrides: ComponentsOverrides<Theme>['CountrySelect'] = {
  root: ({ theme }) => ({
    position: 'fixed',
    width: '100vw',
    bottom: 0,
    left: 0,
    height: theme.spacing(10.5),
    background: theme.palette.purple[700],
    padding: theme.spacing(2)
  }),
  contentContainer: ({ theme }) => ({
    'display': 'flex',
    'justifyContent': 'center',
    'gap': theme.spacing(2),
    'height': '100%',
    'button, a': {
      height: '100%',
      svg: {
        color: theme.palette.common.white
      }
    }
  }),
  message: ({ theme }) => ({
    ...theme.typography.textBase,
    color: theme.palette.common.white,
    display: 'flex',
    alignItems: 'center'
  }),
  countryButton: ({ theme }) => ({
    width: theme.spacing(25),
    border: `2px solid ${theme.palette.purple[600]}`,
    height: '100%',
    color: theme.palette.common.white,
    textAlign: 'left',
    marginRight: theme.spacing(2)
  }),
  closeButton: ({ theme }) => ({
    backgroundColor: theme.palette.purple[600]
  }),
  flag: ({ theme }) => ({
    borderRadius: 0,
    marginRight: theme.spacing(1)
  })
};

// https://mui.com/customization/theme-components/#adding-new-component-variants
const createVariants = (theme: Theme): ComponentsVariants['CountrySelect'] => [];

export default (theme: Theme): ThemeOptions => ({
  components: {
    CountrySelect: {
      defaultProps,
      styleOverrides,
      variants: createVariants(theme)
    }
  }
});
