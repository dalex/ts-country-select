import React from 'react';
import { useRouter } from 'next/router';
import { getCookie } from 'cookies-next';
import { Button, Box, Container, styled, Typography } from '@mui/material';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import CloseIcon from '@mui/icons-material/Close';

// Custom components
import ErrorBoundary from '@project/components/src/ErrorBoundary';
import ContentModule from '@project/components/src/ContentModule';
import Link from '@project/components/src/components/Link';

const CountrySelect = () => {
  const router = useRouter();
  const { locale } = router;

  const countryCookie = getCookie('project_ct') as string;

  const localeMapper: { [key: string]: string } = {
    US: 'en-US',
    AU: 'en-AU',
    GB: 'en-GB',
  };

  const countryCodeMapper: { [key: string]: string } = {
    US: 'United States',
    AU: 'Australia',
    GB: 'United Kingdom',
  };

  const [showCountrySelect, setShowCountrySelect] =
    React.useState<boolean>(false);
  const [visitorCountry, setVisitorCountry] = React.useState<string>(
    countryCookie ? countryCookie : ''
  );
  const [visitorLocale, setVisitorLocale] = React.useState<string | null>(null);
  const [flagImage, setFlagImage] = React.useState<string | null>(null);

  React.useEffect(() => {
    setVisitorCountry(countryCookie);
  }, []);

  React.useEffect(() => {
    setVisitorLocale(localeMapper?.[visitorCountry]);
  }, [visitorCountry]);

  React.useEffect(() => {
    setShowCountrySelect(visitorLocale !== locale);
    setFlagImage(`/locales/${visitorLocale}.svg`);
  }, [visitorLocale, locale]);

  const linkProps = { href: '/', locale: visitorLocale };

  return (
    <>
      {showCountrySelect && (
        <ErrorBoundary>
          <Root>
            <ContentContainer>
              <Message>Select region</Message>
              <Link {...linkProps}>
                <CountryButton variant="button-outlined">
                  <Flag __typename="Media" file={{ url: flagImage }} />
                  {countryCodeMapper?.[visitorCountry]}
                </CountryButton>
                <Button color="primary" variant="contained">
                  <ChevronRightIcon fontSize="large" />
                </Button>
              </Link>
              <CloseButton
                variant="contained"
                onClick={() => setShowCountrySelect(false)}
              >
                <CloseIcon fontSize="large" />
              </CloseButton>
            </ContentContainer>
          </Root>
        </ErrorBoundary>
      )}
    </>
  );
};

const Root = styled(Box, {
  name: 'CountrySelect',
  slot: 'Root',
  overridesResolver: (_, styles) => [styles.root],
})(({}) => ({}));

const ContentContainer = styled(Container, {
  name: 'CountrySelect',
  slot: 'ContentContainer',
  overridesResolver: (_, styles) => [styles.contentContainer],
})(({}) => ({}));

const Message = styled(Typography, {
  name: 'CountrySelect',
  slot: 'Message',
  overridesResolver: (_, styles) => [styles.message],
})(({}) => ({}));

const CountryButton = styled(Link, {
  name: 'CountrySelect',
  slot: 'CountryButton',
  overridesResolver: (_, styles) => [styles.countryButton],
})(({}) => ({}));

const CloseButton = styled(Button, {
  name: 'CountrySelect',
  slot: 'CloseButton',
  overridesResolver: (_, styles) => [styles.closeButton],
})(({}) => ({}));

const Flag = styled(ContentModule, {
  name: 'CountrySelect',
  slot: 'Flag',
  overridesResolver: (_, styles) => [styles.flag],
})(({}) => ({}));

export default CountrySelect;
