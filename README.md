# ts-country-select

Example component using:
- [Typescript](https://www.typescriptlang.org/)
- [NextJs](https://nextjs.org/)
- [MUI](https://mui.com/)


### Notes

- Styles are defined by a local theme file, extending the global theme
- References a cookie, `project_ct`, set on first visit defaulting to `en-US`, and compares it's value to the selected option
- Component extracted from a private repo I'm currently working on defined as `@project`
